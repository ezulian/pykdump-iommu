# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2023 Jerry Snitselaar <jsnitsel@redhat.com>

from pykdump.API import *
from enum import Enum

import bit_manip as bm
import core_iommu as ci

# AMD Capabilities Register

AMD_CAP_CAPEXT_MASK = bm.bit(28)
AMD_CAP_EFRSUP_MASK = bm.bit(27)
AMD_CAP_NPCACHE_MASK = bm.bit(26)
AMD_CAP_HTTUN_MASK = bm.bit(25)
AMD_CAP_IOTLB_MASK = bm.bit(24)
AMD_CAP_CAPREV_SHIFT = 19
AMD_CAP_CAPREV_MASK = bm.gen_mask_ull(23, 19)
AMD_CAP_CAPTYPE_SHIFT = 16
AMD_CAP_CAPTYPE_MASK = bm.gen_mask_ull(18, 16)
AMD_CAP_CAPPTR_SHIFT = 8
AMD_CAP_CAPPTR_MASK = bm.gen_mask_ull(15, 8)
AMD_CAP_CAPID_SHIFT = 0
AMD_CAP_CAPID_MASK = bm.gen_mask_ull(7, 0)

# AMD Extended Features Register

AMD_EFR_SNP_MASK = bm.bit(63)
AMD_EFR_FPDS_MASK = bm.bit(62)
AMD_EFR_GAUDS_MASK = bm.bit(61)
AMD_EFR_VIOMMU_MASK = bm.bit(55)
AMD_EFR_INVIOTLB_MASK = bm.bit(54)
AMD_EFR_HDSUP_MASK = bm.bit(52)
AMD_EFR_ATTRFW_MASK = bm.bit(51)
AMD_EFR_EPHSUP_MASK = bm.bit(50)
AMD_EFR_HASUP_MASK = bm.bit(49)
AMD_EFR_GIOSUP_MASK = bm.bit(48)
AMD_EFR_MSICAP_MASK = bm.bit(46)
AMD_EFR_PEROPT_MASK = bm.bit(45)
AMD_EFR_BLKSTMK_MASK = bm.bit(44)
AMD_EFR_MARCS_SHIFT = 42
AMD_EFR_MARCS_MASK = bm.gen_mask_ull(43, 42)
AMD_EFR_PPRARS_MASK = bm.bit(41)
AMD_EFR_PPROES_MASK = bm.bit(40)
AMD_EFR_DEVTBLSEG_SHIFT = 38
AMD_EFR_DEVTBLSEG_MASK = bm.gen_mask_ull(39, 38)
AMD_EFR_USSUP_MASK = bm.bit(37)
AMD_EFR_PASMX_SHIFT = 32
AMD_EFR_PASMX_MASK = bm.gen_mask_ull(36, 32)
AMD_EFR_SATS_MASK = bm.bit(31)
AMD_EFR_DEVLG_SHIFT = 28
AMD_EFR_DEVLG_MASK = bm.gen_mask_ull(29, 28)
AMD_EFR_DPPRLG_SHIFT = 24
AMD_EFR_DPPRLG_MASK = bm.gen_mask_ull(25, 24)
AMD_EFR_GAMS_SHIFT = 21
AMD_EFR_GAMS_MASK = bm.gen_mask_ull(23, 21)
AMD_EFR_SMIFRC_SHIFT = 18
AMD_EFR_SMIFRC_MASK = bm.gen_mask_ull(20, 18)
AMD_EFR_SMIFS_SHIFT = 16
AMD_EFR_SMIFS_MASK = bm.gen_mask_ull(17, 16)
AMD_EFR_GLXS_SHIFT = 14
AMD_EFR_GLXS_MASK = bm.gen_mask_ull(15, 14)
AMD_EFR_GATS_SHIFT = 12
AMD_EFR_GATS_MASK = bm.gen_mask_ull(13, 12)
AMD_EFR_HATS_SHIFT = 10
AMD_EFR_HATS_MASK = bm.gen_mask_ull(11, 10)
AMD_EFR_PCS_MASK = bm.bit(9)
AMD_EFR_HES_MASK = bm.bit(8)
AMD_EFR_GAS_MASK = bm.bit(7)
AMD_EFR_IAS_MASK = bm.bit(6)
AMD_EFR_GAPPIS_MASK = bm.bit(5)
AMD_EFR_GTS_MASK = bm.bit(4)
AMD_EFR_NXS_MASK = bm.bit(3)
AMD_EFR_XTS_MASK = bm.bit(2)
AMD_EFR_PPRS_MASK = bm.bit(1)
AMD_EFR_PRFS_MASK = bm.bit(0)

# AMD Extended Features Register 2

AMD_EFR2_SNPAVIC_SHIFT = 5
AMD_EFR2_SNPAVIC_MASK = bm.gen_mask_ull(7, 5)
AMD_EFR2_GAPPID_MASK = bm.bit(4)
AMD_EFR2_GCR3TRPMODE_MASK = bm.bit(3)

# AMD IO PGTABLE BITS

AMD_PM_LEVEL_INDEX_MASK_SIZE = 9
AMD_PM_LEVEL_INDEX_MASK = bm.bit_mask(AMD_PM_LEVEL_INDEX_MASK_SIZE, 0) # should be shifted by level shift
AMD_PTE_IW_MASK = bm.bit(62)
AMD_PTE_IR_MASK = bm.bit(61)
AMD_PTE_FC_MASK = bm.bit(60)
AMD_PTE_U_MASK = bm.bit(59)
AMD_PTE_PGADDR_SHIFT = 12
AMD_PTE_PGADDR_MASK = bm.gen_mask_ull(51, 12)
AMD_PTE_MODE_SHIFT = 9
AMD_PTE_MODE_MASK = bm.gen_mask_ull(11, 9)
AMD_PTE_D_MASK = bm.bit(6)
AMD_PTE_A_MASK = bm.bit(5)
AMD_PTE_PR_MASK = bm.bit(0)

# dte.data[3]
AMD_DTE_SNOOP_SHIFT = 56
AMD_DTE_SNOOP_MASK = bm.gen_mask_ull(63, 56)
AMD_DTE_MD0FC_MASK = bm.bit(55)
AMD_DTE_ATTRV_MASK = bm.bit(54)
AMD_DTE_GID_SHIFT = 32
AMD_DTE_GID_MASK = bm.gen_mask_ull(47, 32)
AMD_DTE_GDID_SHIFT = 16
AMD_DTE_GDID_MASK = bm.gen_mask_ull(31, 16)
AMD_DTE_VIMU_MASK = bm.bit(15)
# dte.data[2]
AMD_DTE_LINT1_MASK = bm.bit(63)
AMD_DTE_LINT0_MASK = bm.bit(62)
AMD_DTE_INTCTL_SHIFT = 60
AMD_DTE_INTCTL_MASK = bm.gen_mask_ull(61, 60)
AMD_DTE_HPE_MASK = bm.bit(59)
AMD_DTE_NMI_MASK = bm.bit(58)
AMD_DTE_EINT_MASK = bm.bit(57)
AMD_DTE_INIT_MASK = bm.bit(56)
AMD_DTE_GPM_SHIFT = 54
AMD_DTE_GPM_MASK = bm.gen_mask_ull(55, 54)
AMD_DTE_IRTRP_SHIFT = 6 
AMD_DTE_IRTRP_MASK = bm.gen_mask_ull(51, 6)
AMD_DTE_IG_MASK = bm.bit(5)
AMD_DTE_ITL_SHIFT = 1
AMD_DTE_ITL_MASK = bm.gen_mask_ull(4, 1)
AMD_DTE_IV_MASK = bm.bit(0)
# dte.data[1]
AMD_DTE_GCR3TRP_SHIFT_C = 43
AMD_DTE_GCR3TRP_MASK_C = bm.gen_mask_ull(63, 43)
AMD_DTE_SATS_MASK = bm.bit(42)
AMD_DTE_SYSM_SHIFT = 40
AMD_DTE_SYSM_MASK = bm.gen_mask_ull(41, 40)
AMD_DTE_EX_MASK = bm.bit(39)
AMD_DTE_SD_MASK = bm.bit(38)
AMD_DTE_CACHE_MASK = bm.bit(37)
AMD_DTE_IOCTL_SHIFT = 35
AMD_DTE_IOCTL_MASK = bm.gen_mask_ull(36, 35)
AMD_DTE_SA_MASK = bm.bit(34)
AMD_DTE_SE_MASK = bm.bit(33)
AMD_DTE_IOTLB_MASK = bm.bit(32) 
AMD_DTE_GCR3TRP_SHIFT_B = 16
AMD_DTE_GCR3TRP_MASK_B = bm.gen_mask_ull(31, 16)
AMD_DTE_DOMID_SHIFT = 0
AMD_DTE_DOMID_MASK = bm.gen_mask_ull(15, 0)
# dte.data[0]
AMD_DTE_GCR3TRP_SHIFT_A = 58
AMD_DTE_GCR3TRP_MASK_A = bm.gen_mask_ull(60, 58)
AMD_DTE_GLX_SHIFT = 56
AMD_DTE_GLX_MASK = bm.gen_mask_ull(57, 56)
AMD_DTE_GV_MASK = bm.bit(55)
AMD_DTE_GIOV_MASK = bm.bit(54)
AMD_DTE_GPRP_MASK = bm.bit(53)
AMD_DTE_PPR_MASK = bm.bit(52)
AMD_DTE_HAD_SHIFT = 7
AMD_DTE_HAD_MASK = bm.gen_mask_ull(8, 7)
AMD_DTE_TV_MASK = bm.bit(1)
AMD_DTE_V_MASK = bm.bit(0)

# bits [14:12]
AMD_GCR3TRP_SHIFT_A = 12
# bits [30:15]
AMD_GCR3TRP_SHIFT_B = 15
# bits [31:51] 
AMD_GCR3TRP_SHIFT_C = 31

# io_pagtable_fmt enum in include/linux/io-pgtable.h
AMD_IOMMU_V1 = 6
AMD_IOMMU_V2 = 7

def caps(dev, args):
    di = readSU("struct dev_iommu", dev.iommu)
    idd = readSU("struct iommu_dev_data", di.priv)
    iommuAddr = do_rlookup(di, args)
    iommu = iommuAddr[idd.devid]

    bus, dev, fn = ci.parse_devid(iommu.devid)
    if isinstance(iommu.pci_seg, int):
        pci_seg = iommu.pci_seg
    else:
        pci_seg = iommu.pci_seg.id
        
    print(f"AMD IOMMU {pci_seg:04x}:{bus:x}:{dev:x}.{fn:x} amd_iommu address: {iommu:016x}")
    print(f"AMD IOMMU Capabilities Register: {iommu.cap:016x}")
    print("-------------------------------------------------")
    bm.check_set(iommu.cap, AMD_CAP_CAPEXT_MASK, "Supports IOMMU Miscellanous Information Register")
    bm.check_set(iommu.cap, AMD_CAP_EFRSUP_MASK, "Supports IOMMU Extended Features Register")
    bm.check_set(iommu.cap, AMD_CAP_NPCACHE_MASK, "Not Present Table Entries are cached")
    bm.check_set(iommu.cap, AMD_CAP_HTTUN_MASK, "Supports HyperTransport Tunnel Translation")
    bm.check_set(iommu.cap, AMD_CAP_IOTLB_MASK, "Supports IOTLB (Supports ATS translation request messages)")

    caprev = bm.get_bits_shifted(iommu.cap, AMD_CAP_CAPREV_MASK, AMD_CAP_CAPREV_SHIFT)
    print(f"Capability Revision: {caprev}")

    captype = bm.get_bits_shifted(iommu.cap, AMD_CAP_CAPTYPE_MASK, AMD_CAP_CAPTYPE_SHIFT)
    print(f"IOMMU capability block type: {captype}")

    capptr = bm.get_bits_shifted(iommu.cap, AMD_CAP_CAPPTR_MASK, AMD_CAP_CAPPTR_SHIFT)
    print(f"Capability pointer: {capptr:016x}")

    capid = bm.get_bits_shifted(iommu.cap, AMD_CAP_CAPID_MASK, AMD_CAP_CAPID_SHIFT)
    print(f"Capability ID: {capid:016x}")

    print("")
    print(f"AMD IOMMU Extended Features: {iommu.features:016x}")
    print("---------------------------------------------")
    bm.check_set(iommu.features, AMD_EFR_SNP_MASK, "Supports Secure Nested Paging")
    bm.check_set(iommu.features, AMD_EFR_FPDS_MASK, "Supports Force Physical Destination Mode for Interrupt Remapping")
    bm.check_set(iommu.features, AMD_EFR_GAUDS_MASK, "Supports disabling hardware update on guest page table access bit")
    bm.check_set(iommu.features, AMD_EFR_VIOMMU_MASK, "Supports virtualized IOMMUs")
    bm.check_set(iommu.features, AMD_EFR_INVIOTLB_MASK, "Supports Invalidate IOTLB Type")
    bm.check_set(iommu.features, AMD_EFR_HDSUP_MASK, "Supports Host Dirty")
    bm.check_set(iommu.features, AMD_EFR_ATTRFW_MASK, "Supports Attribute Forwarding")
    bm.check_set(iommu.features, AMD_EFR_EPHSUP_MASK, "Supports Enhanced PPR Handling")
    bm.check_set(iommu.features, AMD_EFR_HASUP_MASK, "Supports Host Access")
    bm.check_set(iommu.features, AMD_EFR_GIOSUP_MASK, "Supports Guest I/O Protection")
    bm.check_set(iommu.features, AMD_EFR_MSICAP_MASK, "Supports MSI Capability Register MMIO access")
    bm.check_set(iommu.features, AMD_EFR_PEROPT_MASK, "Supports IOMMU performance optimization")
    bm.check_set(iommu.features, AMD_EFR_BLKSTMK_MASK, "Supports Block StopMark messages")

    marc = bm.get_bits_shifted(iommu.features, AMD_EFR_MARCS_MASK, AMD_EFR_MARCS_SHIFT)
    if marc == 1:
        print("Supports 4 Memory Access Routing and Control register 3-tuples")
    elif marc == 2:
        print("Supports 8 Memory Access Routing and Control register 3-tuples")

    bm.check_set(iommu.features, AMD_EFR_PPRARS_MASK, "Supports PPR Automatic Response")
    bm.check_set(iommu.features, AMD_EFR_PPROES_MASK, "Supports PPR Log Overflow Early Warning")

    devtblseg = bm.get_bits_shifted(iommu.features, AMD_EFR_DEVTBLSEG_MASK, AMD_EFR_DEVTBLSEG_SHIFT)
    print(f"Supports {2 ** devtblseg} Device Table Segments")

    bm.check_set(iommu.features, AMD_EFR_USSUP_MASK, "Supports User/supervisor page protection")

    pasidmax = bm.get_bits_shifted(iommu.features, AMD_EFR_PASMX_MASK, AMD_EFR_PASMX_SHIFT)
    if bm.bits_check(iommu.features, AMD_EFR_GTS_MASK):
        print("Supports Guest Translation")
        print(f"Supports {pasidmax + 1} bit PASID ids")
    
    bm.check_set(iommu.features, AMD_EFR_SATS_MASK, "Supports Secure ATS")

    devtlog = bm.get_bits_shifted(iommu.features, AMD_EFR_DEVLG_MASK, AMD_EFR_DEVLG_SHIFT)
    if devtlog == 1:
       print("Supports Event log dual duffer without autoswap")
    elif devtlog == 2:
        print("Supports Event log dual buffer with autoswap")

    dpprlog = bm.get_bits_shifted(iommu.features, AMD_EFR_DPPRLG_MASK, AMD_EFR_DPPRLG_SHIFT)
    if dpprlog == 1:
        print("Supports PPR log dual buffer without autoswap")
    elif dpprlog == 2:
        print("Supports PPR log dual buffer with autoswap")
    
    gams = bm.get_bits_shifted(iommu.features, AMD_EFR_GAMS_MASK, AMD_EFR_GAMS_SHIFT)
    if gams == 1:
        print("Supports Guest Virtual APIC mode")

    smifrc = bm.get_bits_shifted(iommu.features, AMD_EFR_SMIFRC_MASK, AMD_EFR_SMIFRC_SHIFT)
    smifs = bm.get_bits_shifted(iommu.features, AMD_EFR_SMIFS_MASK, AMD_EFR_SMIFS_SHIFT)
    if smifs == 0:
        print("SMI Interrupts are passed through")
    elif smifs == 1:
        print("SMI Interrupts are filtered")

    if smifs != 0:
        print(f"Supports {2 ** smifrc} SMI filter registers")

    glxs = bm.get_bits_shifted(iommu.features, AMD_EFR_GLXS_MASK, AMD_EFR_GLXS_SHIFT)
    if glxs == 0:
        print("Supports single-level Guest CR3 base table address translation")
    elif glxs == 1:
        print("Supports two-level Guest CR3 base table address translation")
    elif glxs == 2:
        print("Supports three-level Guest CR3 base table address translation")

    gats = bm.get_bits_shifted(iommu.features, AMD_EFR_GATS_MASK, AMD_EFR_GATS_SHIFT)
    print(f"Guest Address Translation Size: {gats + 4} levels")

    hats = bm.get_bits_shifted(iommu.features, AMD_EFR_HATS_MASK, AMD_EFR_HATS_SHIFT)
    print(f"Host Address Translation Size: {hats + 4} levels")
    
    bm.check_set(iommu.features, AMD_EFR_PCS_MASK, "Supports performance counters")
    bm.check_set(iommu.features, AMD_EFR_HES_MASK, "Supports hardware error registers")
    bm.check_set(iommu.features, AMD_EFR_GAS_MASK, "Supports Guest Virtual APIC")
    bm.check_set(iommu.features, AMD_EFR_IAS_MASK, "Supports Invalidate IOMMU All")
    bm.check_set(iommu.features, AMD_EFR_GAPPIS_MASK, "Supports Guest APIC Physical Processor Interrupts")
    bm.check_set(iommu.features, AMD_EFR_NXS_MASK, "Supports No-execute protection and privilege level")
    bm.check_set(iommu.features, AMD_EFR_XTS_MASK, "Supports x2APIC")
    bm.check_set(iommu.features, AMD_EFR_PPRS_MASK, "Supports Peripheral page requests")
    bm.check_set(iommu.features, AMD_EFR_PRFS_MASK, "Supports Prefetch")

    print("")
    if iommu.hasField("features2"):
        gam = bm.get_bits_shifted(iommu.features2, AMD_EFR2_SNPAVIC_MASK, AMD_EFR2_SNPAVIC_SHIFT)
        print(f"AMD IOMMU Extended Features 2 Register ({iommu.features2:016x})")
        print("------------------------------------")
        if gam != 1:
            print("Virtual AVIC not supported with SNP")
        bm.check_set(iommu.features2, AMD_EFR2_GAPPID_MASK, "Supports masking Guest APIC Physical Processor interrupts")
        bm.check_set(iommu.features2, AMD_EFR2_GCR3TRPMODE_MASK, "Support for GPA based GCR3 pointer inside DTE")
    else:
        print("This AMD IOMMU has no Extended Features 2 Register")
        print("")
    print("")
        

# AMD IO Page Table helper functions

def amd_iommu_pte_present(pte):
    return bm.bits_check(pte, AMD_PTE_PR_MASK)
        

def amd_iommu_ptov(paddr, offset):
    smeMeMask = readSymbol("sme_me_mask")
    return (paddr & ~smeMeMask) + offset


def amd_iommu_vtop(vaddr, offset):
    smeMeMask = readSymbol("sme_me_mask")
    return (vaddr - offset) | smeMeMask


def amd_page_address(pte):
    return bm.get_bits(pte, AMD_PTE_PGADDR_MASK)


def amd_iommu_pte_page(pte, offset):
    return amd_iommu_ptov(amd_page_address(pte), offset)


def amd_pm_level_shift(level):
    return 12 + (level * AMD_PM_LEVEL_INDEX_MASK_SIZE)


def amd_pm_level_size(level):
    if level < 6:
        return (1 << amd_pm_level_shift(level)) - 1
    return 0xffffffffffffffff

def amd_pte_level_page_size(level):
    return 1 << (12 + (AMD_PM_LEVEL_INDEX_MASK_SIZE * level))


def amd_pm_pte_level(pte):
    return bm.get_bits_shifted(pte, AMD_PTE_MODE_MASK, AMD_PTE_MODE_SHIFT)


def amd_pm_level_index(level, iova):
    shift = amd_pm_level_shift(level)
    return bm.get_bits_shifted(iova, (AMD_PM_LEVEL_INDEX_MASK << shift), shift)

def amd_index_adjust(level, iova):
    return 8 * amd_pm_level_index(level, iova)    


def amd_next_pte_addr(pte, iova, offset):
    level = amd_pm_pte_level(pte) - 1
    nextPte = amd_iommu_pte_page(pte, offset)
    return nextPte + amd_index_adjust(level, iova)


def amd_get_gcr3trp(dte):
    gcr3trp = 0
    gcr3trp |= (bm.get_bits_shifted(dte.data[0], AMD_DTE_GCR3TRP_MASK_A, AMD_DTE_GCR3TRP_SHIFT_A) << AMD_GCR3TRP_SHIFT_A)
    gcr3trp |= (bm.get_bits_shifted(dte.data[1], AMD_DTE_GCR3TRP_MASK_B, AMD_DTE_GCR3TRP_SHIFT_B) << AMD_GCR3TRP_SHIFT_B)
    gcr3trp |= (bm.get_bits_shifted(dte.data[1], AMD_DTE_GCR3TRP_MASK_C, AMD_DTE_GCR3TRP_SHIFT_C) << AMD_GCR3TRP_SHIFT_C)
    return gcr3trp


# If the pte next level is set to 7 a page size can be encoding in
# the address, and found by determining the first 0 bit.
# See Table 14 in section 2.2.3 of AMD I/O Virtualization Technology Specification 3.07 (page 78)
def amd_pte_page_size(pte):
    return 1 << (1 + bm.ffz((pte | 0xfff)))


def amd_page_size_pte_count(size):
    return 1 << ((bm.ffs(size) - 12) % 9)


def amd_pte_page_size_paddr(pte, iova, size):
    smeMeMask = readSymbol("sme_me_mask")
    offset_mask = size - 1
    paddr = bm.get_bits(pte, AMD_PTE_PGADDR_MASK)
    paddr &= ~smeMeMask
    paddr &= ~offset_mask
    paddr |= (iova & offset_mask)
    return paddr


def amd_page_size_pte(address, size, args):
    smeMeMask = readSymbol("sme_me_mask")
    pte = ((address | smeMeMask) | (size - 1)) & (~(size >> 1) & AMD_PTE_PGADDR_MASK)
    return pte


def AMDPagingModes(level):
    if level == 0:
        return "Translation Disabled (Access controlled by IR and IW bits. SPA=GPA)"
    elif level == 1:
        return "1 Level Page Table (provides a 21-bit GPA space)"
    elif level == 2:
        return "2 Level Page Table (provides a 30-bit GPA space)"
    elif level == 3:
        return "3 Level Page Table (provides a 39-bit GPA space)"
    elif level == 4:
        return "4 Level Page Table (provides a 48-bit GPA space)"
    elif level == 5:
        return "5 Level Page Table (provides a 57-bit GPA space)"
    elif level == 6:
        return "6 Level Page Table (provides a 64-bit GPA space)"
    elif level == 7:
        return "Reserved"

AMD_BASIC_IRTE_VECTOR_SHIFT = 16
AMD_BASIC_IRTE_VECTOR_MASK = bm.gen_mask_u32(23, 16)
AMD_BASIC_IRTE_DEST_MASK = bm.gen_mask_u32(15, 8)
AMD_BASIC_IRTE_DEST_SHIFT = 8
AMD_BASIC_IRTE_GM_MASK = bm.bit_u32(7)
AMD_BASIC_IRTE_DM_MASK = bm.bit_u32(6)
AMD_BASIC_IRTE_EOI_MASK = bm.bit_u32(5)
AMD_BASIC_IRTE_TYPE_SHIFT = 2
AMD_BASIC_IRTE_TYPE_MASK = bm.gen_mask_u32(4, 2)
AMD_BASIC_IRTE_SIOPF_MASK = bm.bit_u32(1)
AMD_BASIC_IRTE_RME_MASK = bm.bit_u32(0)

def basic_irte(addr):
    irte = readU32(addr)
    vector = bm.get_bits_shifted(irte, AMD_BASIC_IRTE_VECTOR_MASK, AMD_BASIC_IRTE_VECTOR_SHIFT)
    dest = bm.get_bits_shifted(irte, AMD_BASIC_IRTE_DEST_MASK, AMD_BASIC_IRTE_DEST_SHIFT)
    gm = bm.bits_check(irte, AMD_BASIC_IRTE_GM_MASK)
    dm = bm.bits_check(irte, AMD_BASIC_IRTE_DM_MASK)
    eoi = bm.bits_check(irte, AMD_BASIC_IRTE_EOI_MASK)
    int_type = bm.get_bits_shifted(irte, AMD_BASIC_IRTE_TYPE_MASK, AMD_BASIC_IRTE_TYPE_SHIFT)
    siopf = bm.bits_check(irte, AMD_BASIC_IRTE_SIOPF_MASK)
    rme = bm.bits_check(irte, AMD_BASIC_IRTE_RME_MASK)
    if rme:
        print(f' irte: {irte:08x} @ {addr:016x} vector: {vector:2x} dest: {dest:2x} dm: {"L" if dm else "P":1} eoi: {eoi:x} type: {"F" if int_type == 0 else "A":1} siopf: {"S" if siopf else "L":1}')


# irte[1]
AMD_GA_IRTE_VAPIC_TRP_SHIFT = 12
AMD_GA_IRTE_VAPIC_TRP_MASK = bm.gen_mask_ull(51, 12)
AMD_GA_IRTE_VECTOR_SHIFT = 0
AMD_GA_IRTE_VECTOR_MASK = bm.gen_mask_ull(7,0)
# irte[0]
AMD_GA_IRTE_GATAG_SHIFT = 32
AMD_GA_IRTE_GATAG_MASK = bm.gen_mask_ull(63, 32)
AMD_GA_IRTE_DEST_SHIFT = 8
AMD_GA_IRTE_DEST_MASK = bm.gen_mask_ull(15, 8)
AMD_GA_IRTE_GM_MASK = bm.bit(7)
AMD_GA_IRTE_DM_MASK = bm.bit(6)
AMD_GA_IRTE_ISRUN_MASK = bm.bit(6)
AMD_GA_IRTE_EOI_MASK = bm.bit(5)
AMD_GA_IRTE_GAPPID_MASK = bm.bit(5)
AMD_GA_IRTE_TYPE_SHIFT = 2
AMD_GA_IRTE_TYPE_MASK = bm.gen_mask_ull(4, 2)
AMD_GA_IRTE_GALOG_MASK = bm.bit(2)
AMD_GA_IRTE_SIOP_MASK = bm.bit(1)
AMD_GA_IRTE_RME_MASK = bm.bit(0)


def ga_irte(addr):
    irte = [readU64(addr), readU64(addr+8)]

    irtestr = f'{irte[1]:016x}:{irte[0]:016x}'
    vector = bm.get_bits_shifted(irte[1], AMD_GA_IRTE_VECTOR_MASK, AMD_GA_IRTE_VECTOR_SHIFT)
    dest = bm.get_bits_shifted(irte[0], AMD_GA_IRTE_DEST_MASK, AMD_GA_IRTE_DEST_SHIFT)
    siopf = bm.bits_check(irte[0], AMD_GA_IRTE_SIOP_MASK)
    rme = bm.bits_check(irte[0], AMD_GA_IRTE_RME_MASK)
    gm = bm.bits_check(irte[0], AMD_GA_IRTE_GM_MASK)
    if not rme:
        return
    if gm:
        isrun = bm.bits_check(irte[0], AMD_GA_IRTE_ISRUN_MASK)
        gappid = bm.bits_check(irte[0], AMD_GA_IRTE_GAPPID_MASK)
        galog = bm.bits_check(irte[0], AMD_GA_IRTE_GALOG_MASK)
        gatag = bm.get_bits_shifted(irte[0], AMD_GA_IRTE_GATAG_MASK, AMD_GA_IRTE_GATAG_SHIFT)
        vapic_rtp = bm.get_bits_shifted(irte[1], AMD_GA_IRTE_VAPIC_TRP_MASK, AMD_GA_IRTE_VAPIC_TRP_SHIFT)
        print(f' irte: {irtestr:32} @ {addr:016x} vector: {vector:2x} dest: {dest:2x} isrun: {isrun:x} gappid: {gappid:x} galog: {galog:x} siopf: {"S" if siopf else "L":1} gatag: {gatag:08x} vapic RTP: {vapic_rtp:016x}')
    else:
        dm = bm.bits_check(irte[0], AMD_GA_IRTE_DM_MASK)
        eoi = bm.bits_check(irte[0], AMD_GA_IRTE_EOI_MASK)
        int_type = bm.get_bits_shifted(irte[0], AMD_GA_IRTE_TYPE_MASK, AMD_GA_IRTE_TYPE_SHIFT)
        print(f' irte: {irtestr:32} @ {addr:016x} vector: {vector:2x} dest: {dest:2x} dm: {"L" if dm else "P":1} eoi {eoi:x} type: {"F" if int_type == 0 else "A":1} siopf: {"S" if siopf else "L":1}')


AMD_XT_IRTE_DEST_SHIFT = 24
# irte[1]            
AMD_XT_IRTE_DEST2_SHIFT = 56
AMD_XT_IRTE_DEST2_MASK = bm.gen_mask_ull(63, 56)
AMD_XT_IRTE_VAPIC_TRP_SHIFT = 12
AMD_XT_IRTE_VAPIC_TRP_MASK = bm.gen_mask_ull(51, 12)
AMD_XT_IRTE_VECTOR_SHIFT = 0
AMD_XT_IRTE_VECTOR_MASK = bm.gen_mask_ull(7, 0)
# irte[0]
AMD_XT_IRTE_GATAG_SHIFT = 32
AMD_XT_IRTE_GATAG_MASK = bm.gen_mask_ull(63, 32)
AMD_XT_IRTE_DEST1_SHIFT = 8
AMD_XT_IRTE_DEST1_MASK = bm.gen_mask_ull(31, 8)
AMD_XT_IRTE_GM_MASK = bm.bit(7)
AMD_XT_IRTE_DM_MASK = bm.bit(6)
AMD_XT_IRTE_ISRUN_MASK = bm.bit(6)
AMD_XT_IRTE_EOI_MASK = bm.bit(5)
AMD_XT_IRTE_GAPPID_MASK = bm.bit(5)
AMD_XT_IRTE_TYPE_SHIFT = 2
AMD_XT_IRTE_TYPE_MASK = bm.gen_mask_ull(4, 2)
AMD_XT_IRTE_GALOG_MASK = bm.bit(2)
AMD_XT_IRTE_SIOP_MASK = bm.bit(1)
AMD_XT_IRTE_RME_MASK = bm.bit(0)


def xt_irte(addr):
    irte = [readU64(addr), readU64(addr+8)]

    irtestr = f'{irte[1]:016x}:{irte[0]:016x}'
    dest2 = bm.get_bits_shifted(irte[1], AMD_XT_IRTE_DEST2_MASK, AMD_XT_IRTE_DEST2_SHIFT)
    dest1 = bm.get_bits_shifted(irte[0], AMD_XT_IRTE_DEST1_MASK, AMD_XT_IRTE_DEST1_SHIFT)
    dest = dest2 << AMD_XT_IRTE_DEST_SHIFT
    dest |= dest1
    vector = bm.get_bits_shifted(irte[1], AMD_XT_IRTE_VECTOR_MASK, AMD_XT_IRTE_VECTOR_SHIFT)
    siopf = bm.bits_check(irte[0], AMD_XT_IRTE_SIOP_MASK)
    rme = bm.bits_check(irte[0], AMD_XT_IRTE_RME_MASK)
    gm = bm.bits_check(irte[0], AMD_XT_IRTE_GM_MASK)
    if not rme:
        return
    if gm:
        isrun = bm.bits_check(irte[0], AMD_XT_IRTE_ISRUN_MASK)
        gappid = bm.bits_check(irte[0], AMD_XT_IRTE_GAPPID_MASK)
        galog = bm.bits_check(irte[0], AMD_XT_IRTE_GALOG_MASK)
        gatag = bm.get_bits_shifted(irte[0], AMD_XT_IRTE_GATAG_MASK, AMD_XT_IRTE_GATAG_SHIFT)
        vapic_rtp = bm.get_bits_shifted(irte[1], AMD_Xt_IRTE_VAPIC_TRP_MASK, AMD_XT_IRTE_VAPIC_TRP_SHIFT)
        print(f' irte: {irtestr:32} @ {addr:016x} vector: {vector:2x} dest: {dest:08x} isrun: {isrun:x} gappid: {gappid:x} galog: {galog:x} siopf: {"S" if siopf else "L":1} gatag: {gatag:08x} vapic RTP: {vapic_rtp:016x}')
    else:
        dm = bm.bits_check(irte[0], AMD_XT_IRTE_DM_MASK)
        eoi = bm.bits_check(irte[0], AMD_XT_IRTE_EOI_MASK)
        int_type = bm.get_bits_shifted(irte[0], AMD_XT_IRTE_TYPE_MASK, AMD_XT_IRTE_TYPE_SHIFT)
        print(f' irte: {irtestr:32} @ {addr:016x} vector: {vector:2x} dest: {dest:08x} dm: {"L" if dm else "P":1} eoi {eoi:x} type: {"F" if int_type == 0 else "A":1} siopf: {"S" if siopf else "L":1}')


def dump_irte(dev, args):
    di = readSU("struct dev_iommu", dev.iommu)
    idd = readSU("struct iommu_dev_data", di.priv)
    pd = readSU("struct protection_domain", idd.domain)
    rlookup = do_rlookup(di, args)
    iommu = rlookup[idd.devid]
    dlookup = do_devtable_lookup(di, args)
    dte = dlookup[idd.devid]

    intctl = bm.get_bits_shifted(dte.data[2], AMD_DTE_INTCTL_MASK, AMD_DTE_INTCTL_SHIFT)
    if intctl != 2:
        print("interrupts are not remapped")
        return

    irtrp = bm.get_bits(dte.data[2], AMD_DTE_IRTRP_MASK)
    irtrp = amd_iommu_ptov(irtrp, args.pageOffsetBase)
    irtlen = bm.get_bits_shifted(dte.data[2], AMD_DTE_ITL_MASK, AMD_DTE_ITL_SHIFT)
    irtlen = 2 ** irtlen

    gam = bm.get_bits_shifted(iommu.features, AMD_EFR_GAMS_MASK, AMD_EFR_GAMS_SHIFT)
    gas = bm.bits_check(iommu.features, AMD_EFR_GAS_MASK)
    xt = readSymbol("amd_iommu_xt_mode")
    ir = readSymbol("amd_iommu_guest_ir")

    if not gas or ir == 0:
        irte_size = 4
    else:
        irte_size = 16

    print(f"dev_iommu: {di:016x} iommu_dev_data: {idd:016x} protection_domain: {pd:016x} amd_iommu: {iommu:016x}")
    print(f'interrupt remapping table root pointer: {irtrp:016x}')
    if gas and xt:
        print(f"x2apic enabled")
    print(f'----------------------------------------------------')

    for x in range(irtlen):
        irteaddr = irtrp + x*irte_size
        if not gas:
            basic_irte(irteaddr)
        elif not xt:
            ga_irte(irteaddr)
        else:
            xt_irte(irteaddr)


def dump_dte_cmd(dev, args):
    di = readSU("struct dev_iommu", dev.iommu)
    idd = readSU("struct iommu_dev_data", di.priv)
    pd = readSU("struct protection_domain", idd.domain)
    rlookup = do_rlookup(di, args)
    iommu = rlookup[idd.devid]
    dlookup = do_devtable_lookup(di, args)
    dte = dlookup[idd.devid]

    if not bm.bits_check(dte.data[0], AMD_DTE_V_MASK):
        print("Device Table Entry is not marked Valid")
        return
    else:
        print(f"DTE: {dte.data[3]:016x}:{dte.data[2]:016x}:{dte.data[1]:016x}:{dte.data[0]:016x}")
        print("------------------------------------------------------------------------")
        dump_dte(dte, args)

    
def dump_dte(dte, args):
    bm.check_set_else(dte.data[3], AMD_DTE_ATTRV_MASK, " Mode0FC and Snoop Attribute fields are valid",
                   " Mode0FC and Snoop Attribute fields are not valid")
    if bm.bits_check(dte.data[3], AMD_DTE_ATTRV_MASK):
        print(f" Snoop Attribute: {bm.get_bits_shifted(dte.data[3], AMD_DTE_SNOOP_MASK, AMD_DTE_SNOOP_SHIFT):x}")
        print(f" Mode0FC: {1 if bm.bits_check(dte.data[3], AMD_DTE_MD0FC_MASK) else 0}")


    gid = bm.get_bits_shifted(dte.data[3], AMD_DTE_GID_MASK, AMD_DTE_GID_SHIFT)
    print(f" GuestID: {gid:x}")
    gdid = bm.get_bits_shifted(dte.data[3], AMD_DTE_GDID_MASK, AMD_DTE_GDID_SHIFT)
    print(f" Guest DeviceID: {gdid:x}")

    bm.check_set_else(dte.data[3], AMD_DTE_VIMU_MASK, " Device assigned under virtualized IOMMU in a guest",
                   " Device assigned under real IOMMU")
    bm.check_set_else(dte.data[2], AMD_DTE_LINT1_MASK, " Device initiated LINT1 interrupts are forwarded unmapped",
                   " Device initiated LINT1 interrupts are target aborted")
    bm.check_set_else(dte.data[2], AMD_DTE_LINT0_MASK, " Device initiated LINT0 interrupts are forwarded unmapped",
                   " Device initiated LINT0 interrupts are target aborted")
    bm.check_set_else(dte.data[2], AMD_DTE_HPE_MASK, " Host page table data structures guaranteed pinned and present",
                   " Host page table data structures may be pageable. Devices may need to issue PPR to access the memory")
    bm.check_set_else(dte.data[2], AMD_DTE_NMI_MASK, " Pass through NMI interrupt messages unmapped",
                   " NMI interrupt messages are target aborted")
    bm.check_set_else(dte.data[2], AMD_DTE_EINT_MASK, " Pass through ExtInt interrupt messages unmapped",
                   " ExtInt interrupt messages are target aborted")
    bm.check_set_else(dte.data[2], AMD_DTE_INIT_MASK, " Pass through INIT interrupt messages unmapped",
                   " INIT interrupt messages are target aborted")

    if bm.bits_check(dte.data[0], AMD_DTE_GV_MASK):
        gpm = bm.get_bits_shifted(dte.data[2], AMD_DTE_GPM_MASK, AMD_DTE_GPM_SHIFT)
        if gpm == 0:
            print(" Guest Page Table Start Level: 4")
        elif gpm == 1:
            print(" Guest Page Table Start Level: 5")

    intctl = bm.get_bits_shifted(dte.data[2], AMD_DTE_INTCTL_MASK, AMD_DTE_INTCTL_SHIFT)
    if intctl == 0:
        print(" Fixed and arbitrated interrupts target aborted")
    elif intctl == 1:
        print(" Fixed and arbitrated interrupts are forwarded unmapped")
    elif intctl == 2:
        print(" Fixed and arbitrated interrupts are remapped")
        irtrp = bm.get_bits(dte.data[2], AMD_DTE_IRTRP_MASK)
        print(f" Interrupt Table Root Pointer: {irtrp:016x}")
        print(f" Interrupt Table Root Pointer vaddr: {amd_iommu_ptov(irtrp, args.pageOffsetBase):016x}")

    bm.check_set_else(dte.data[2], AMD_DTE_IG_MASK, " Ignores unmapped interrupts", " Unmapped interrupt causes IO_PAGE_FAULT event to be logged")
    irqtablen = bm.get_bits_shifted(dte.data[2], AMD_DTE_ITL_MASK, AMD_DTE_ITL_SHIFT)
    print(f" Interrupt Remapping Table Length: {2 ** irqtablen}")
    bm.check_set_else(dte.data[2], AMD_DTE_IV_MASK, " Interrupt map is valid", " Interrupt map is not valid")
    bm.check_set_else(dte.data[1], AMD_DTE_SATS_MASK, " ATS requests are processed as secure ATS requests",
                   " ATS requests are processed as non-secure ATS requests")

    sysmgt = bm.get_bits_shifted(dte.data[1], AMD_DTE_SYSM_MASK, AMD_DTE_SYSM_SHIFT)
    if sysmgt == 0:
        print(" Device initiated DMA transactions in the system management address range are returned target abort. Translation requests return target abort")
    elif sysmgt == 1:
        print(" Device initiated system management messages, including INTx messages, are forwarded untranslated. Upstream reads and non-posted writes return target abort. Translation requests return target abort")
    elif sysmgt == 2:
        print(" Device initiated INTx messages are forwarded by the IOMMU untranslated. Other system management requests return target abort. Translation requests return target abort")
    elif sysmgt == 3:
        print(" Device initiated DMA transactions in the system management address range are translated by the IOMMU")

    bm.check_set_else(dte.data[1], AMD_DTE_EX_MASK, " Accesses from this device to the IOMMU exclusion range are excluded from translation and access checks",
                   " Accesses from this device to the IOMMU exclusion range are subject to translation and access checks")
    bm.check_set_else(dte.data[1], AMD_DTE_SD_MASK, " IOMMU page table walk transcations are not snooped for this device",
                   " IOMMU page table walk transcations are snooped for this device")
    bm.check_set_else(dte.data[1], AMD_DTE_CACHE_MASK, " IOMMU avoids caching GPA-SPA translation information obtained for ATS requests",
                   " IOMMU caches GPA-SPA translation information obtained for ATS requests")     

    portio = bm.get_bits_shifted(dte.data[1], AMD_DTE_IOCTL_MASK, AMD_DTE_IOCTL_SHIFT)
    if portio == 0:
        print(" Device-initiated port I/O is not allowed")
    elif portio == 1:
        print(" Device-initiated port I/O space transactions are allowed. IOMMU passes port I/O through untranslated. Translation requests aborted")
    elif portio == 2:
        print(" Device-initiated port I/O transactions are allowed. IOMMU translates as memory transations")

    print(f" Suppress all I/O page fault events: {1 if bm.bits_check(dte.data[1], AMD_DTE_SA_MASK) else 0}")
    print(f" Suppress I/O page fault events: {1 if bm.bits_check(dte.data[1], AMD_DTE_SE_MASK) else 0}")
    bm.check_set_else(dte.data[1], AMD_DTE_IOTLB_MASK, " IOMMU responds to ATS requests", " IOMMU returns target abort for ATS requests")
    print(f" GCR3 Table Root Pointer: {amd_get_gcr3trp(dte):016x}")
    print(f' Domain ID: 0x{bm.get_bits_shifted(dte.data[1], AMD_DTE_DOMID_MASK, AMD_DTE_DOMID_SHIFT):x}')
    print(f" I/O Write Permission: {1 if bm.bits_check(dte.data[0], AMD_PTE_IW_MASK) else 0}")
    print(f" I/O Read Permission: {1 if bm.bits_check(dte.data[0], AMD_PTE_IR_MASK) else 0}")
    print(f" Guest Levels Translated: {bm.get_bits_shifted(dte.data[0], AMD_DTE_GLX_MASK, AMD_DTE_GLX_SHIFT) + 1}")
    print(f" Guest Translation Valid: {1 if bm.bits_check(dte.data[0], AMD_DTE_GV_MASK) else 0}")
    print(f" Guest I/O Protection Valid: {1 if bm.bits_check(dte.data[0], AMD_DTE_GIOV_MASK) else 0}")
    print(f" Guest PPR Response with PASID: {1 if bm.bits_check(dte.data[0], AMD_DTE_GPRP_MASK) else 0}")
    print(f" PPR Enable: {1 if bm.bits_check(dte.data[0], AMD_DTE_PPR_MASK) else 0}")
    ptrpAddr = amd_page_address(dte.data[0])
    print(f" IO Page Table Root Pointer: {ptrpAddr:016x}")
    ptrpVaddr = amd_iommu_pte_page(ptrpAddr, args.pageOffsetBase)
    print(f" IOPTRP vaddr: {ptrpVaddr:016x}")
    level = amd_pm_pte_level(dte.data[0])
    print(f" Paging Mode: {AMDPagingModes(level)}")
    print(f" Host Access Dirty: {1 if bm.bits_check(dte.data[0], AMD_DTE_HAD_MASK) else 0}")
    print(f" Translation Information Valid: {1 if bm.bits_check(dte.data[0], AMD_DTE_TV_MASK) else 0}")
    print(f" Valid: {1 if bm.bits_check(dte.data[0], AMD_DTE_V_MASK) else 0}")
    print("")


def PagingModeEntries(level):
    if level == 7 or level == 0:
        return "PTE"
    else:
        return "PDE"
    

def do_rlookup(dev_iommu, args):
    idd = readSU("struct iommu_dev_data", dev_iommu.priv)
    pd = readSU("struct protection_domain", idd.domain)
    try:
        lookup = readSymbol("amd_iommu_rlookup_table")
    except TypeError:
        # okay switched to segmented pci lookup
        amd_seg_list = ListHead(sym2addr("amd_iommu_pci_seg_list"), "struct amd_iommu_pci_seg")
        pciDev = readSU("struct pci_dev", args.pcidev)
        sysd = readSU("struct pci_sysdata", pciDev.sysdata)
        for sl in amd_seg_list.list:
            if sl.id == sysd.domain:
                seg = sl
                break
        lookup = seg.rlookup_table

    return lookup


def do_devtable_lookup(dev_iommu, args):
    idd = readSU("struct iommu_dev_data", dev_iommu.priv)
    pd = readSU("struct protection_domain", idd.domain)
    try:
        lookup = readSymbol("amd_iommu_dev_table")
    except TypeError:
        # okay switched to segmented pci lookup
        amd_seg_list = ListHead(sym2addr("amd_iommu_pci_seg_list"), "struct amd_iommu_pci_seg")
        pciDev = readSU("struct pci_dev", args.pcidev)
        sysd = readSU("struct pci_sysdata", pciDev.sysdata)
        for sl in amd_seg_list.list:
            if sl.id == sysd.domain:
                seg = sl
                break
        lookup = seg.dev_table
    
    return lookup
    

def v2_walk(dev, args):
    print("v2 page table walk not implemented")


def v1_one_walk(ptrp, mode, iova, iommu, args):
    pageSize = 0
    prevLevel = mode
    level = prevLevel - 1
    index = amd_pm_level_index(level, iova)
    pteLoc = ptrp + index*8
    pte = readU64(pteLoc)
    page_size = amd_pte_level_page_size(level)

    print(f"Walking iova: {iova:016x}")
    print("------------------------------")
    print(f"{level} Root ({pteLoc:016x}): {pte:016x}")
    if not amd_iommu_pte_present(pte):
        print(f"Root Table entry marked not Present")
        return -1
    while level >= 0:
        nextLevel = amd_pm_pte_level(pte)            
        if nextLevel == 7 or nextLevel == 0:
            if nextLevel == 7:
                l7pte = 0
                tmpsz = amd_pte_page_size(pte)
                if tmpsz != -1:
                    page_size = tmpsz
                    l7pte = amd_page_size_pte_count(page_size)

            print(f"{nextLevel}  {PagingModeEntries(level):4} ({pteLoc:016x}): {pte:016x}")
        elif prevLevel != mode:
            print(f"{level} {PagingModeEntries(level):4} ({pteLoc:016x}): {pte:016x}")

        # not PR
        if not amd_iommu_pte_present(pte):
            print("   PTE marked not present")
            return page_size

        # No Level Skipping
        if nextLevel != 7 and nextLevel != level:
            print("Level skipping not supported")
            return -1

        if (nextLevel == 7 or nextLevel == 0) or args.verbose:
            print(f"  -----------------------------------------")
            print(f"   Page Size: {page_size:016x}")
            if nextLevel == 7:
                print(f"   Non-Default Page Size PTE: {l7pte} L7 ptes")
            bm.check_set_else(pte, AMD_PTE_IW_MASK, "   Write Permission Enabled", "   Write Permission Disabled")
            bm.check_set_else(pte, AMD_PTE_IR_MASK, "   Read Permission Enabled", "   Read Permission Disabled")
            if nextLevel == 7:
                pgmask = page_size - 1
                paddr = amd_pte_page_size_paddr(pte, iova, page_size)
                print(f"   Physical Page Address: {paddr:016x} L7 base: {(paddr & ~pgmask):016x}")
            else:
                paddr = amd_page_address(pte)
                print(f"   Physical Page Address: {paddr:016x}")
            print(f"         virtual address: {amd_iommu_ptov(paddr, args.pageOffsetBase):016x}")
            
            if nextLevel == 7 or nextLevel == 0:

                bm.check_set_else(pte, AMD_PTE_FC_MASK, "   FC: IOMMU sets coherent attribute", "   FC: IOMMU passes coherent attribute") 
                if bm.bits_check(iommu.features, AMD_EFR_USSUP_MASK):
                    bm.check_set_else(pte, AMD_PTE_U_MASK, "   U bit set", "   U bit not set")

                    if args.dirty:
                        bm.check_set_else(pte, AMD_PTE_A_MASK, "   Accessed bit set", "   Accessed bit clear") 
                        bm.check_set_else(pte, AMD_PTE_D_MASK, "   Page marked dirty", "   Page not marked dirty")

                return page_size

        # Get Next Level Entry
        prevLevel = level
        level -= 1
        pteLoc = amd_next_pte_addr(pte, iova, args.pageOffsetBase)
        pte = readU64(pteLoc)
        page_size = amd_pte_level_page_size(level)
        print("")


def v1_walk(dev, args):
    iova = args.iova
    dumpDTE = args.dte
    di = readSU("struct dev_iommu", dev.iommu)
    idd = readSU("struct iommu_dev_data", di.priv)
    pd = readSU("struct protection_domain", idd.domain)
    rlookup = do_rlookup(di, args)
    iommu = rlookup[idd.devid]
    dlookup = do_devtable_lookup(di, args)
    dte = dlookup[idd.devid]
    dirty = False

    if bm.bits_check(iommu.features, AMD_EFR_HDSUP_MASK) and bm.get_bits(dte.data[0], AMD_DTE_HAD_MASK):
        dirty = True

    print(f"dev_iommu: {di:016x} iommu_dev_data: {idd:016x} protection_domain: {pd:016x} amd_iommu: {iommu:016x}")
    # Dump DTE
    if not bm.bits_check(dte.data[0], AMD_DTE_V_MASK):
        print("Device Table Entry is not marked Valid")
        return
    else:
        print(f"DTE: {dte.data[3]:016x}:{dte.data[2]:016x}:{dte.data[1]:016x}:{dte.data[0]:016x}")
        print("------------------------------------------------------------------------")
        if dumpDTE:
            dump_dte(dte, args)

    ptrpAddr = amd_page_address(dte.data[0])
    ptrpVaddr = amd_iommu_pte_page(ptrpAddr, args.pageOffsetBase)
    mode = amd_pm_pte_level(dte.data[0])

    if args.verbose:
        print(f"Page Table Root Pointer Address: {ptrpAddr:016x}")
        print(f"Page Table Root Pointer Virtual Address: {ptrpVaddr:016x}")
        print(f"Page Table Mode: {mode}")
        print("")

    print("")

    # check for bad combinations of iommu dma domain and DTE translation valid bit
    if bm.bits_check(dte.data[0], AMD_DTE_TV_MASK) and (pd.domain.type == ci.IOMMU_DOMAIN_IDENTITY):
        print("DTE has Translation Valid bit set, but the dma domain is type Identity")
        return
    elif not bm.bits_check(dte.data[0], AMD_DTE_TV_MASK) and (pd.domain.type & ci.__IOMMU_DOMAIN_PAGING == ci.__IOMMU_DOMAIN_PAGING):
        print("DTE has Translation Valid bit cleared, but the dma domain is has __IOMMU_DOMAIN_PAGING bit set")
        return

    print(f"IOMMU Domain type: {ci.domain_type_str(pd.domain.type)}")
    # If this is an identity domain we can bail now
    if (pd.domain.type == ci.IOMMU_DOMAIN_IDENTITY):
        print("the IO page tables aren't used for identity domains")
        return

    levelSize = amd_pm_level_size(mode)
    print("")
    if iova > levelSize:
        print(f"iova {iova:016x} > PM Level Size: {levelSize:016x}")
        return

    # Walk Table
    if args.mapsz is None:
        v1_one_walk(ptrpVaddr, mode, iova, iommu, args)
    else:
        end = iova + args.mapsz
        while iova < end:
            sz = v1_one_walk(ptrpVaddr, mode, iova, iommu, args)
            if sz == -1:
                return
            else:
                iova += sz

def walk(dev, args):
    if args.io_pgtable_fmt == AMD_IOMMU_V1:
        v1_walk(dev, args)
    else:
        v2_walk(dev, args)

def v2_search(dev, args):
    print("v2 page table search not implemented")

def v1_table_search(addr, level, iova, iommu, paddr, depth, args):
    pte = readU64(addr)

    prevLevel = level
    curLevel = level - 1
    # check valid/present
    if not amd_iommu_pte_present(pte):
        return

    nextLevel = amd_pm_pte_level(pte)

    if args.verbose:
        if nextLevel == 7:
            l7pte = 0
            tmpsz = amd_pte_page_size(pte)
            if tmpsz != -1:
                page_size = tmpsz
                l7pte = amd_page_size_pte_count(page_size)

            pad = " " * 7
            l7pte_paddr = amd_pte_page_size_paddr(pte, iova, page_size)
            pgmask = page_size - 1
            print(f"{pad}checking L7 pte: {pte:016x} @ {addr:016x} level: {nextLevel} iova: {iova:016x} (paddr: {l7pte_paddr:x} base: {(l7pte_paddr & ~pgmask):x} page_size: {page_size} # entries: {l7pte})")
        elif nextLevel == 0:
            pad = " " * 21
            print(f"{pad}checking pte: {pte:016x} @ {addr:016x} level: {nextLevel} iova: {iova:016x} (paddr: {amd_page_address(pte):016x})")
        else:
            pad = " " * (3 * (7 - nextLevel))
            print(f"{pad}checking pde: {pte:016x} @ {addr:016x} level: {nextLevel}")

    if nextLevel == 7 or nextLevel == 0:
        if nextLevel == 7:
            tmpsz = amd_pte_page_size(pte)
            if tmpsz != -1:
                page_size = tmpsz

            pte_paddr = amd_pte_page_size_paddr(pte, iova, page_size)
        else:
            page_size = amd_pte_level_page_size(curLevel)
            pte_paddr = amd_page_address(pte)

        pgmask = page_size - 1

        if paddr:
            if (paddr & ~pgmask) == (pte_paddr & ~pgmask):
                if nextLevel == 7:
                    pad = " " * 7
                    print(f"{pad}pte address: {addr:016x} pte: {pte:016x} iova {iova:016x} maps {paddr:016x} L7 page size: {page_size} L7 base: {(pte_paddr & ~pgmask):016x}!")
                else:
                    pad = " " * (7 - nextLevel)
                    print(f"{pad}pte address: {addr:016x} pte: {pte:016x} iova {iova:016x} maps {paddr:x}!")
        elif args.dirty:            
            if bm.bits_check(pte, AMD_PTE_D_MASK):
                if nextLevel == 7:
                    pad = " " * 7
                    print(f"{pad}pte address: {addr:016x} pte: {pte:016x} iova {iova:016x} page size: {page_size} base: {(pte_paddr & ~pgmask):x} marked Dirty")
                else:
                    pad = " " * (7 - nextLevel)
                    print(f"{pad}pte address: {addr:016x} pte: {pte:016x} iova {iova:016x} marked Dirty")
        elif args.access:
            if bm.bits_check(pte, AMD_PTE_A_MASK):
                if nextLevel == 7:
                    pad = " " * 7
                    print(f"{pad}pte address: {addr:016x} pte: {pte:016x} iova {iova:016x} page size: {page_size} base: {(pte_paddr & ~pgmask):x} marked Accessed")
                else:
                    pad = " " * (7 - nextLevel)
                    print(f"{pad}pte address: {addr:016x} pte: {pte:016x} iova {iova:016x} marked Accessed")
            
        return

    for x in range(511, -1, -1):
        next_addr = amd_iommu_pte_page(pte, args.pageOffsetBase)
        next_addr += x*8
        if nextLevel == 7:
            masked_index = x & AMD_PM_LEVEL_INDEX_MASK
            shifted_index = masked_index << amd_pm_level_shift(prevLevel - 1)
            clear_iova = iova & ~(AMD_PM_LEVEL_INDEX_MASK << amd_pm_level_shift(prevLevel - 1))
            adjusted_iova = clear_iova | shifted_index
            aligned_iova = iova | (page_size - 1)
            lpg_mask = ((~(page_size >> 1)) & AMD_PTE_PGADDR_MASK)
            iova = aligned_iova & lpg_mask
        elif nextLevel == 0:
            masked_index = x & AMD_PM_LEVEL_INDEX_MASK
            shifted_index = masked_index << amd_pm_level_shift(0)
            clear_iova = iova & ~(AMD_PM_LEVEL_INDEX_MASK << amd_pm_level_shift(0))
            iova = (clear_iova | (masked_index << amd_pm_level_shift(0)))
        else:
            masked_index = x & AMD_PM_LEVEL_INDEX_MASK
            shifted_index = masked_index << amd_pm_level_shift(curLevel)
            clear_iova = iova & ~(AMD_PM_LEVEL_INDEX_MASK << amd_pm_level_shift(curLevel))
            iova = (clear_iova | (masked_index << amd_pm_level_shift(curLevel)))
        v1_table_search(next_addr, curLevel, iova, iommu, paddr, depth + 1, args)


def v1_search(dev, args):
    if args.paddr is not None:
        paddr_page = args.paddr & ~0xfff
    else:
        paddr_page = 0

    di = readSU("struct dev_iommu", dev.iommu)
    idd = readSU("struct iommu_dev_data", di.priv)
    pd = readSU("struct protection_domain", idd.domain)
    rlookup = do_rlookup(di, args)
    iommu = rlookup[idd.devid]
    dlookup = do_devtable_lookup(di, args)
    dte = dlookup[idd.devid]
    iova = 0

    dumpDTE = args.dte
    if dumpDTE and not bm.bits_check(dte.data[0], AMD_DTE_V_MASK):
        print("Device Table Entry is not marked Valid")
        return
    elif dumpDTE:
        print(f"DTE: {dte.data[3]:016x}:{dte.data[2]:016x}:{dte.data[1]:016x}:{dte.data[0]:016x}")
        print("------------------------------------------------------------------------")
        dump_dte(dte, args)

    ptrpAddr = amd_page_address(dte.data[0])
    ptrpVaddr = amd_iommu_pte_page(ptrpAddr, args.pageOffsetBase)
    mode = amd_pm_pte_level(dte.data[0])

    print(f"dev_iommu: {di:016x} iommu_dev_data: {idd:016x} protection_domain: {pd:016x} amd_iommu: {iommu:016x}")
    if args.verbose:
        print(f"mode: {mode}")
    level = mode - 1

    if args.dirty or args.access:
        if not bm.bits_check(iommu.features, AMD_EFR_HDSUP_MASK):
            print("Host Dirty support is not supported by this iommu")
            return
        elif bm.get_bits(dte.data[0], AMD_DTE_HAD_MASK) == 0:
            print("Host Access Dirty is not enabled")
            return

    # check for bad combinations of iommu dma domain and DTE translation valid bit
    if bm.bits_check(dte.data[0], AMD_DTE_TV_MASK) and (pd.domain.type == ci.IOMMU_DOMAIN_IDENTITY):
        print("DTE has Translation Valid bit set, but the dma domain is type Identity")
        return
    elif not bm.bits_check(dte.data[0], AMD_DTE_TV_MASK) and (pd.domain.type & ci.__IOMMU_DOMAIN_PAGING == ci.__IOMMU_DOMAIN_PAGING):
        print("DTE has Translation Valid bit cleared, but the dma domain is has __IOMMU_DOMAIN_PAGING bit set")
        return

    # If this is an identity domain we can bail now
    if (pd.domain.type == ci.IOMMU_DOMAIN_IDENTITY):
        print("the IO page tables aren't used for identity domains")
        return

    if args.verbose:
        if paddr_page != 0:
            print(f"searching for address: {paddr_page:016x}")
            print("")

    # Though there are 512 possible entries in the root table page, it is only possible for 128 of them to be used
    # if mode is 6 (bits 63:57 would index into it)
    for x in range(127 if mode == 6 else 511, -1, -1):
        pteLoc = ptrpVaddr + x*8
        iova = (x & AMD_PM_LEVEL_INDEX_MASK) << amd_pm_level_shift(level)
        v1_table_search(pteLoc, level, iova, iommu, paddr_page, 1, args)


def search(dev, args):
    if args.io_pgtable_fmt == AMD_IOMMU_V1:
        v1_search(dev, args)
    else:
        v2_search(dev, args)


def dump_tables(dev, args):
    args.verbose = True
    args.paddr = 0x0
    search(dev, args)

        
def parse_pte(dev, args):
    pte = args.pte
    di = readSU("struct dev_iommu", dev.iommu)
    idd = readSU("struct iommu_dev_data", di.priv)
    pd = readSU("struct protection_domain", idd.domain)
    rlookup = do_rlookup(di, args)
    iommu = rlookup[idd.devid]
    dlookup = do_devtable_lookup(di, args)
    dte = dlookup[idd.devid]

    if amd_iommu_pte_present(pte):
        print("entry valid. Present bit set.")
    else:
        print("entry not valid. Present bit clear.")
        return

    level = amd_pm_pte_level(pte)
    # actual PTE ? If not 0 or 7, then it is a PDE
    if level == 7 or level == 0:
        # Large Page Size?
        if level == 7:
            page_size = amd_pte_page_size(pte)
            if args.iova is None:
                iova = 0
            else:
                iova = args.iova
            paddr = amd_pte_page_size_paddr(pte, iova, page_size)
            if paddr & (page_size - 1) != 0:
                print(f"address {paddr:016x} is not aligned for page size: {page_size}")
            new_pte = amd_page_size_pte(paddr, page_size, args)
            print(f"rebuilt PTE with no permissions set: {new_pte:016x}")
            print(f"L7 page size: {page_size}")
        else:
            paddr = amd_page_address(pte)
        print(f"physical address in PTE: {paddr:016x}")
        bm.check_set_else(pte, AMD_PTE_IW_MASK, "   Write Permission Enabled", "   Write Permission Disabled")
        bm.check_set_else(pte, AMD_PTE_IR_MASK, "   Read Permission Enabled", "   Read Permission Disabled")
        bm.check_set_else(pte, AMD_PTE_FC_MASK, "   FC: IOMMU sets coherent attribute", "   FC: IOMMU passes coherent attribute") 
        if bm.bits_check(iommu.features, AMD_EFR_USSUP_MASK):
            bm.check_set_else(pte, AMD_PTE_U_MASK, "   U bit set", "   U bit not set")

            if bm.bits_check(iommu.features, AMD_EFR_HDSUP_MASK) and bm.get_bits(dte.data[0], AMD_DTE_HAD_MASK):
                bm.check_set_else(pte, AMD_PTE_A_MASK, "   Accessed bit set", "   Accessed bit clear") 
                bm.check_set_else(pte, AMD_PTE_D_MASK, "   Page marked dirty", "   Page not marked dirty")
    else:
        print(f"PDE level: {level}")
