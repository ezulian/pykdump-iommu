# What is pykdump-iommu

This is a pykdump script for looking at things related to IOMMUs
in a kernel vmcore.

* Search io pagtables for a device to see if a physical address is mapped
* Walk mapping of an io virtual address for a device
* Dump all present ptes in io pagetables for a device
* Dump & parse capabilities registers for IOMMU associated with device

This is something that was created to assist myself in looking
at some problems. So it is not being very actively maintained, and gets
worked on when I have time, or I'm in the mood.

# Usage

Requires extending crash with [pykdump](https://sourceforge.net/projects/pykdump/), and loading a vmcore with the crash command:

```
crash> extend mpykdumpx86_64.so
/usr/lib64/crash/extensions/mpykdumpx86_64.so: shared object loaded
```

Options:

```
usage: iommu.py [-h] [-w] [-c] [-v] [-s] [-d] [-p] (--device PTR-ADDR | --pcidev PTR-ADDR) [--paddr PADDR | --dirty | --access] [--pte PTE] [--iova IOVA] [--size MAPPING-SIZE] [--dte]

optional arguments:
  -h, --help           show this help message and exit
  -w                   Walk io page tables for an iova within the given device's domain
  -c, --capabilities   Parse iommu capabilities registers
  -v, --verbose        give verbose output
  -s                   walk valid mappings for the device and search for address
  -d, --dump           Dump Present Page Table Entries
  -p, --parse-pte      Parse PTE
  --device PTR-ADDR    hex address of a device struct for device attached to iommu
  --pcidev PTR-ADDR    hex address of a pci_dev struct for device attached to iommu
  --paddr PADDR        hex physical address to search for in mappings
  --dirty              Find ptes marked with dirty bit
  --access             Find ptes marked with access bit
  --pte PTE            value of pte entry to parse
  --iova IOVA          hex iova address to walk io page tables for
  --size MAPPING-SIZE  total size (in hex) of mapping to walk starting with iova
  --dte                Parse and dump Device Table Entry (AMD)
```

# Examples

Dumping Device Table Entry (AMD)

```
> epython iommu.py --dte --pcidev ffff93c0121ef000
DTE: 0000000000000000:20000001602c0013:0000000000000019:60000001785acc03
------------------------------------------------------------------------
 Mode0FC and Snoop Attribute fields are not valid
 GuestID: 0
 Guest DeviceID: 0
 Device assigned under real IOMMU
 Device initiated LINT1 interrupts are target aborted
 Device initiated LINT0 interrupts are target aborted
 Host page table data structures may be pageable. Devices may need to issue PPR to access the memory
 NMI interrupt messages are target aborted
 ExtInt interrupt messages are target aborted
 INIT interrupt messages are target aborted
 Fixed and arbitrated interrupts are remapped
 Interrupt Table Root Pointer: 00000001602c0000
 Interrupt Table Root Pointer vaddr: ffff93c0602c0000
 Unmapped interrupt causes IO_PAGE_FAULT event to be logged
 Interrupt Remapping Table Length: 512
 Interrupt map is valid
 ATS requests are processed as non-secure ATS requests
 Device initiated DMA transactions in the system management address range are returned target abort. Translation requests return target abort
 Accesses from this device to the IOMMU exclusion range are subject to translation and access checks
 IOMMU page table walk transcations are snooped for this device
 IOMMU caches GPA-SPA translation information obtained for ATS requests
 Device-initiated port I/O is not allowed
 Suppress all I/O page fault events: 0
 Suppress I/O page fault events: 0
 IOMMU returns target abort for ATS requests
 GCR3 Table Root Pointer: 0000000000000000
 Domain ID: 0x19
 I/O Write Permission: 1
 I/O Read Permission: 1
 Guest Levels Translated: 1
 Guest Translation Valid: 0
 Guest I/O Protection Valid: 0
 Guest PPR Response with PASID: 0
 PPR Enable: 0
 IO Page Table Root Pointer: 00000001785ac000
 IOPTRP vaddr: ffff93c0785ac000
 Paging Mode: 6 Level Page Table (provides a 64-bit GPA space)
 Host Access Dirty: 0
 Translation Information Valid: 1
 Valid: 1
```

Dump Capabilities/Extended Feature Registers:

```
> epython iommu.py -c --pcidev ffff93c0121ef000
AMD IOMMU 0000:40:0.2 amd_iommu address: ffff93c00004b800
AMD IOMMU Capabilities Register: 00000000190b640f
-------------------------------------------------
Supports IOMMU Miscellanous Information Register
Supports IOMMU Extended Features Register
Supports IOTLB (Supports ATS translation request messages)
Capability Revision: 1
IOMMU capability block type: 3
Capability pointer: 0000000000000064
Capability ID: 000000000000000f

AMD IOMMU Extended Features: 058f77ef22294ade
---------------------------------------------
Supports virtualized IOMMUs
Supports Attribute Forwarding
Supports Enhanced PPR Handling
Supports Host Access
Supports Guest I/O Protection
Supports MSI Capability Register MMIO access
Supports IOMMU performance optimization
Supports Block StopMark messages
Supports 4 Memory Access Routing and Control register 3-tuples
Supports PPR Automatic Response
Supports PPR Log Overflow Early Warning
Supports 8 Device Table Segments
Supports User/supervisor page protection
Supports Guest Translation
Supports 16 bit PASID ids
Supports Event log dual buffer with autoswap
Supports PPR log dual buffer with autoswap
Supports Guest Virtual APIC mode
SMI Interrupts are filtered
Supports 4 SMI filter registers
Supports two-level Guest CR3 base table address translation
Guest Address Translation Size: 4 levels
Host Address Translation Size: 6 levels
Supports performance counters
Supports Guest Virtual APIC
Supports Invalidate IOMMU All
Supports No-execute protection and privilege level
Supports x2APIC
Supports Peripheral page requests

This AMD IOMMU has no Extended Features 2 Register
```

Walk io page tables for IO Virtual Address

```
> epython iommu.py -w --pcidev ffff93c0121ef000 --iova fffff000
dev_iommu: ffff93c01263e5a0 iommu_dev_data: ffff93c768e50a80 protection_domain: ffff93c0123a8600 amd_iommu: ffff93c00004b800
DTE: 0000000000000000:20000001602c0013:0000000000000019:60000001785acc03
------------------------------------------------------------------------

IOMMU Domain type: DMA-FQ

Walking iova: 00000000fffff000
------------------------------
5 Root (ffff93c0785ac000): 6000000184de8a01

4 PDE  (ffff93c084de8000): 60000008b26e7801

3 PDE  (ffff93c7b26e7000): 600000010a5a2601

2 PDE  (ffff93c00a5a2018): 600000010f6ba401

1 PDE  (ffff93c00f6baff8): 600000010f6bb201

0  PTE  (ffff93c00f6bbff8): 700000010f6b9001
  -----------------------------------------
   Page Size: 0000000000001000
   Write Permission Enabled
   Read Permission Enabled
   Physical Page Address: 000000010f6b9000
         virtual address: ffff93c00f6b9000
   FC: IOMMU sets coherent attribute
   U bit not set
```

Dump IO page tables for device

```
> epython iommu.py -d --pcidev ffff93c0121ef000
dev_iommu: ffff93c01263e5a0 iommu_dev_data: ffff93c768e50a80 protection_domain: ffff93c0123a8600 amd_iommu: ffff93c00004b800
mode: 6
      checking pde: 6000000104186a01 @ ffff93c0785ac3f8 level: 5
         checking pde: 6000000892b4e801 @ ffff93c004186ff8 level: 4
            checking pde: 600000088b7f2601 @ ffff93c792b4eff8 level: 3
               checking pde: 6000000899780401 @ ffff93c78b7f2ff8 level: 2
                  checking pde: 6000000873287201 @ ffff93c799780ff8 level: 1
                     checking pte: 50000008ad3bb001 @ ffff93c773287ff8 level: 0 iova: fffffffffffff000 (paddr: 00000008ad3bb000)
                     checking pte: 50000008ad3bd001 @ ffff93c773287ff0 level: 0 iova: ffffffffffffe000 (paddr: 00000008ad3bd000)
                     checking pte: 50000008ad3bc001 @ ffff93c773287fe8 level: 0 iova: ffffffffffffd000 (paddr: 00000008ad3bc000)
                     checking pte: 50000008ae631001 @ ffff93c773287fe0 level: 0 iova: ffffffffffffc000 (paddr: 00000008ae631000)
                     checking pte: 50000008ae630001 @ ffff93c773287fd8 level: 0 iova: ffffffffffffb000 (paddr: 00000008ae630000)
                     checking pte: 50000008cbb45001 @ ffff93c773287fd0 level: 0 iova: ffffffffffffa000 (paddr: 00000008cbb45000)
                     checking pte: 50000008cbb44001 @ ffff93c773287fc8 level: 0 iova: ffffffffffff9000 (paddr: 00000008cbb44000)
                     checking pte: 50000008b5d0d001 @ ffff93c773287fc0 level: 0 iova: ffffffffffff8000 (paddr: 00000008b5d0d000)
                     checking pte: 50000008b5d0c001 @ ffff93c773287fb8 level: 0 iova: ffffffffffff7000 (paddr: 00000008b5d0c000)
                     checking pte: 50000008c269d001 @ ffff93c773287fb0 level: 0 iova: ffffffffffff6000 (paddr: 00000008c269d000)
                     checking pte: 50000008c269c001 @ ffff93c773287fa8 level: 0 iova: ffffffffffff5000 (paddr: 00000008c269c000)
                     checking pte: 50000008e0993001 @ ffff93c773287fa0 level: 0 iova: ffffffffffff4000 (paddr: 00000008e0993000)
                     checking pte: 50000008e0992001 @ ffff93c773287f98 level: 0 iova: ffffffffffff3000 (paddr: 00000008e0992000)
                     checking pte: 50000008d8565001 @ ffff93c773287f90 level: 0 iova: ffffffffffff2000 (paddr: 00000008d8565000)
                     checking pte: 50000008d8564001 @ ffff93c773287f88 level: 0 iova: ffffffffffff1000 (paddr: 00000008d8564000)
                     checking pte: 50000008ae59d001 @ ffff93c773287f80 level: 0 iova: ffffffffffff0000 (paddr: 00000008ae59d000)
...
                     checking pte: 50000008aff72001 @ ffff93c7d2698248 level: 0 iova: 0000000000049000 (paddr: 00000008aff72000)
                     checking pte: 500000089eded001 @ ffff93c7d2698240 level: 0 iova: 0000000000048000 (paddr: 000000089eded000)
                     checking pte: 500000089edec001 @ ffff93c7d2698238 level: 0 iova: 0000000000047000 (paddr: 000000089edec000)
                     checking pte: 50000008c339b001 @ ffff93c7d2698230 level: 0 iova: 0000000000046000 (paddr: 00000008c339b000)
                     checking pte: 50000008c339a001 @ ffff93c7d2698228 level: 0 iova: 0000000000045000 (paddr: 00000008c339a000)
                     checking pte: 50000008bb44f001 @ ffff93c7d2698220 level: 0 iova: 0000000000044000 (paddr: 00000008bb44f000)
                     checking pte: 50000008bb44e001 @ ffff93c7d2698218 level: 0 iova: 0000000000043000 (paddr: 00000008bb44e000)
                     checking pte: 50000008cab43001 @ ffff93c7d2698210 level: 0 iova: 0000000000042000 (paddr: 00000008cab43000)
                     checking pte: 50000008cab42001 @ ffff93c7d2698208 level: 0 iova: 0000000000041000 (paddr: 00000008cab42000)
                     checking pte: 500000089d349001 @ ffff93c7d2698200 level: 0 iova: 0000000000040000 (paddr: 000000089d349000)
```

Search to see if physical address is mapped by device (AMD L7 PTE example)

```
> epython iommu.py -s --pcidev ffff93c0121ef000 --paddr 165067000
dev_iommu: ffff93c01263e5a0 iommu_dev_data: ffff93c768e50a80 protection_domain: ffff93c0123a8600 amd_iommu: ffff93c00004b800
       pte address: ffff93c7a13b87d8 pte: 7000000165065e01 iova 00000000c38fb000 maps 0000000165067000 L7 page size: 16384 L7 base: 0000000165064000!
       pte address: ffff93c7a13b87d0 pte: 7000000165065e01 iova 00000000c38fa000 maps 0000000165067000 L7 page size: 16384 L7 base: 0000000165064000!
       pte address: ffff93c7a13b87c8 pte: 7000000165065e01 iova 00000000c38f9000 maps 0000000165067000 L7 page size: 16384 L7 base: 0000000165064000!
       pte address: ffff93c7a13b87c0 pte: 7000000165065e01 iova 00000000c38f8000 maps 0000000165067000 L7 page size: 16384 L7 base: 0000000165064000!
```

Search to see if physical address is mapped to device

```
> epython iommu.py -s --pcidev ffff93c0121ef000 --paddr 8b5bf6000
dev_iommu: ffff93c01263e5a0 iommu_dev_data: ffff93c768e50a80 protection_domain: ffff93c0123a8600 amd_iommu: ffff93c00004b800
       pte address: ffff93c7732876e0 pte: 50000008b5bf6001 iova ffffffffffedc000 maps 8b5bf6000!
```

Parse IO PTE (AMD)

```
> epython iommu.py -p --pcidev ffff93c0121ef000 --pte 500000089d349001
entry valid. Present bit set.
physical address in PTE: 000000089d349000
   Write Permission Enabled
   Read Permission Disabled
   FC: IOMMU sets coherent attribute
   U bit not set
```

Parse L7 IO PTE (AMD)

```
> epython iommu.py -p --pcidev ffff93c0121ef000 --pte 7000000165065e01
entry valid. Present bit set.
rebuilt PTE with no permissions set: 0000000165065000
L7 page size: 16384
physical address in PTE: 0000000165064000
   Write Permission Enabled
   Read Permission Enabled
   FC: IOMMU sets coherent attribute
   U bit not set
```

# Current Status

* AMD
  * parse & dump capabilities registers
  * Dump Device Table Entry for a pci device
  * walk/dump/search v1 page tables

* Intel
  * parse & dump capabilities registers

# TODO

* General
  * Nested / SVA support
  * iommufd (vfio as well?)
  * Interrupt Remapping info
  * iommu groups

* AMD
  * walk/search/dump v2 io pagetables

* Intel
  * legacy and scalable mode page tables
  * search/walk/dump io pagetables

* ARM
  * parse & dump capabilities registers
  * search/walk/dump io pagetables

# License

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.
