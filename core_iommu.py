# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2023 Jerry Snitselaar <jsnitsel@redhat.com>

import bit_manip as bm

__IOMMU_DOMAIN_PAGING = bm.bit_mask(1, 0)
__IOMMU_DOMAIN_DMA_API = bm.bit_mask(1, 1)
__IOMMU_DOMAIN_PT = bm.bit_mask(1, 2)
__IOMMU_DOMAIN_DMA_FQ = bm.bit_mask(1, 3)
__IOMMU_DOMAIN_SVA = bm.bit_mask(1, 4)

IOMMU_DOMAIN_BLOCKED = 0
IOMMU_DOMAIN_IDENTITY = __IOMMU_DOMAIN_PT
IOMMU_DOMAIN_UNMANAGED = __IOMMU_DOMAIN_PAGING
IOMMU_DOMAIN_DMA = (__IOMMU_DOMAIN_DMA_API | __IOMMU_DOMAIN_PAGING)
IOMMU_DOMAIN_DMA_FQ = (__IOMMU_DOMAIN_DMA_API | __IOMMU_DOMAIN_PAGING | __IOMMU_DOMAIN_DMA_FQ)
IOMMU_DOMAIN_SVA = __IOMMU_DOMAIN_SVA


DEVID_BUS_SHIFT = 8
DEVID_BUS_MASK = bm.gen_mask_ull(15, DEVID_BUS_SHIFT)
DEVID_DEV_SHIFT = 3
DEVID_DEV_MASK = bm.gen_mask_ull(7, DEVID_DEV_SHIFT)
DEVID_FN_SHIFT = 0
DEVID_FN_MASK = bm.gen_mask_ull(2, DEVID_FN_SHIFT)

def parse_devid(devid):
    bus = bm.get_bits_shifted(devid, DEVID_BUS_MASK, DEVID_BUS_SHIFT)
    dev = bm.get_bits_shifted(devid, DEVID_DEV_MASK, DEVID_DEV_SHIFT)
    fn = bm.get_bits_shifted(devid, DEVID_FN_MASK, DEVID_FN_SHIFT)

    return (bus, dev, fn)


def domain_type_str(dtype):
    if dtype == IOMMU_DOMAIN_BLOCKED:
        return "Blocked"
    elif dtype == IOMMU_DOMAIN_IDENTITY:
        return "Identity"
    elif dtype == IOMMU_DOMAIN_UNMANAGED:
        return "Unmanaged"
    elif dtype == IOMMU_DOMAIN_DMA:
        return "DMA"
    elif dtype == IOMMU_DOMAIN_DMA_FQ:
        return "DMA-FQ"
    else:
        return "Unknown"

