# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2023 Jerry Snitselaar <jsnitsel@redhat.com>

from pykdump.API import *
from enum import Enum
import bit_manip as bm
import core_iommu as ci



# Intel Capability Register

INTEL_CAP_ESRTPS_MASK = bm.bit(63)
INTEL_CAP_ESIRTPS_MASK = bm.bit(62)
INTEL_CAP_ECMDS_MASK = bm.bit(61)
INTEL_CAP_FS5LP_MASK = bm.bit(60)
INTEL_CAP_PI_MASK = bm.bit(59)
INTEL_CAP_FS1GP_MASK = bm.bit(56)
INTEL_CAP_DRD_MASK = bm.bit(55)
INTEL_CAP_DWD_MASK = bm.bit(54)
INTEL_CAP_MAMV_SHIFT = 48
INTEL_CAP_MAMV_MASK = bm.gen_mask_ull(53, INTEL_CAP_MAMV_SHIFT)
INTEL_CAP_NFR_MASK = bm.gen_mask_ull(47, 40)
INTEL_CAP_PSI_MASK = bm.bit(39)
INTEL_CAP_SSLPS_SHIFT = 34
INTEL_CAP_SSLPS_MASK = bm.gen_mask_ull(37, INTEL_CAP_SSLPS_SHIFT)
INTEL_CAP_FRO_SHIFT = 24
INTEL_CAP_FRO_MASK = bm.gen_mask_ull(33, INTEL_CAP_FRO_SHIFT)
INTEL_CAP_ZLR_MASK = bm.bit(22)
INTEL_CAP_MGAW_SHIFT = 16
INTEL_CAP_MGAW_MASK = bm.gen_mask_ull(21, INTEL_CAP_MGAW_SHIFT)
INTEL_CAP_SAGAW_SHIFT = 8
INTEL_CAP_SAGAW_MASK = bm.gen_mask_ull(12, INTEL_CAP_SAGAW_SHIFT)
INTEL_CAP_CM_MASK = bm.bit(7)
INTEL_CAP_PHMR_MASK = bm.bit(6)
INTEL_CAP_PLMR_MASK = bm.bit(5)
INTEL_CAP_RWBF_MASK = bm.bit(4)
INTEL_CAP_ND_SHIFT = 0
INTEL_CAP_ND_MASK = bm.gen_mask_ull(2, INTEL_CAP_ND_SHIFT)

# Intel Extended Capabilities Register

INTEL_ECAP_SMS_MASK = bm.bit(58)
INTEL_ECAP_RPRIVS_MASK = bm.bit(53)
INTEL_ECAP_ADMS_MASK = bm.bit(52)
INTEL_ECAP_PMS_MASK = bm.bit(51)
INTEL_ECAP_RPS_MASK = bm.bit(49)
INTEL_ECAP_SMPWCS_MASK = bm.bit(48)
INTEL_ECAP_FSTS_MASK = bm.bit(47)
INTEL_ECAP_SSTS_MASK = bm.bit(46)
INTEL_ECAP_SSADS_MASK = bm.bit(45)
INTEL_ECAP_VCS_MASK = bm.bit(44)
INTEL_ECAP_SMTS_MASK = bm.bit(43)
INTEL_ECAP_PDS_MASK = bm.bit(42)
INTEL_ECAP_DIT_MASK = bm.bit(41)
INTEL_ECAP_PASID_MASK = bm.bit(40)
INTEL_ECAP_PSS_SHIFT = 35
INTEL_ECAP_PSS_MASK = bm.gen_mask_ull(39, INTEL_ECAP_PSS_SHIFT)
INTEL_ECAP_EAFS_MASK = bm.bit(34)
INTEL_ECAP_NWFS_MASK = bm.bit(33)
INTEL_ECAP_SRS_MASK = bm.bit(32)
INTEL_ECAP_ERS_MASK = bm.bit(30)
INTEL_ECAP_PRS_MASK = bm.bit(29)
INTEL_ECAP_NEST_MASK = bm.bit(26)
INTEL_ECAP_MTS_MASK = bm.bit(25)
INTEL_ECAP_MHMW_SHIFT = 20
INTEL_ECAP_MHMV_MASK = bm.gen_mask_ull(23, INTEL_ECAP_MHMW_SHIFT)
INTEL_ECAP_IRO_SHIFT = 8
INTEL_ECAP_IRO_MASK = bm.gen_mask_ull(17, INTEL_ECAP_IRO_SHIFT)
INTEL_ECAP_SC_MASK = bm.bit(7)
INTEL_ECAP_PT_MASK = bm.bit(6)
INTEL_ECAP_EIM_MASK = bm.bit(4)
INTEL_ECAP_IR_MASK = bm.bit(3)
INTEL_ECAP_DT_MASK = bm.bit(2)
INTEL_ECAP_QI_MASK = bm.bit(1)
INTEL_ECAP_C_MASK = bm.bit(0)


def caps(dev, args):
    di = readSU("struct dev_iommu", dev.iommu)
    ddi = readSU("struct device_domain_info", di.priv)
    iommu = readSU("struct intel_iommu", ddi.iommu)

    print(f'intel_iommu address: {ddi.iommu:016x}')
    print(f'Capability Register: {iommu.cap:016x} Extended Capability Register: {iommu.ecap:016x}')
    print(f'-----------------------------------------------------------------------------------')    

    bm.check_set(iommu.cap, INTEL_CAP_ESRTPS_MASK, "Supports Enhanced Set Root Table Pointer")
    bm.check_set(iommu.cap, INTEL_CAP_ESIRTPS_MASK, "Supports Enhanced Set Interrupt Remap Table Pointer")
    bm.check_set(iommu.cap, INTEL_CAP_ECMDS_MASK, "Supports Enhanced Commands")
    bm.check_set(iommu.cap, INTEL_CAP_FS5LP_MASK, "Supports 5-level Page Tables for 1st stage translation")
    bm.check_set(iommu.cap, INTEL_CAP_PI_MASK, "Supports Posted Interrupts")
    bm.check_set(iommu.cap, INTEL_CAP_FS1GP_MASK, "Supports 1st stage 1GB Pages")
    bm.check_set(iommu.cap, INTEL_CAP_DRD_MASK, "Supports Read Draining")
    bm.check_set(iommu.cap, INTEL_CAP_DWD_MASK, "Supports Write Draining")

    if bm.bits_check(iommu.cap, INTEL_CAP_PSI_MASK):
        print(f'Supports Page Selective Invalidation')
        print(f'Maximum Address Mask: {bm.get_bits_shifted(iommu.cap, INTEL_CAP_MAMV_MASK, INTEL_CAP_MAMV_SHIFT):016x}')

    sslp = bm.get_bits_shifted(iommu.cap, INTEL_CAP_SSLPS_MASK, INTEL_CAP_SSLPS_SHIFT)
    if sslp & 1:
        print("Supports 2MB Second Stage Large Pages")
    if sslp & 3:
        print("Supports 1GB Second Stage Large Pages")

    print(f'Fault Register Offset: {bm.get_bits_shifted(iommu.cap, INTEL_CAP_FRO_MASK, INTEL_CAP_FRO_SHIFT):016x}')

    bm.check_set(iommu.cap, INTEL_CAP_ZLR_MASK, "Supports Zero Length Reads")
    print(f'Maximum Guest Address Width: {bm.get_bits_shifted(iommu.cap, INTEL_CAP_MGAW_MASK, INTEL_CAP_MGAW_SHIFT)+1}')

    sagaw = bm.get_bits_shifted(iommu.cap, INTEL_CAP_SAGAW_MASK, INTEL_CAP_SAGAW_SHIFT)
    if sagaw & 2:
        print('Supports 39-bit AGAW (3-level Page Table)')
    if sagaw & 4:
        print('Supports 48-bit AGAW (4-level Page Table)')
    if sagaw & 8:
        print('Supports 57-bit AGAW (5-level Page Table)')

    bm.check_set(iommu.cap, INTEL_CAP_CM_MASK, "Supports Caching Mode")
    bm.check_set(iommu.cap, INTEL_CAP_PHMR_MASK, "Supports Protected High Memory Region")
    bm.check_set(iommu.cap, INTEL_CAP_PLMR_MASK, "Supports Protected Low Memory Region")
    nd = 4 + 2*bm.get_bits_shifted(iommu.cap, INTEL_CAP_ND_MASK, INTEL_CAP_ND_SHIFT)
    print(f'Supports {nd}-bit domain-ids, with support for up to {2**nd} domains')

    print("")

    bm.check_set(iommu.ecap, INTEL_ECAP_SMS_MASK, "Supports Stop Marker Message")
    bm.check_set(iommu.ecap, INTEL_ECAP_RPRIVS_MASK, "Supports RID_PRIV field in scalable-mode context entry")
    bm.check_set(iommu.ecap, INTEL_ECAP_ADMS_MASK, "Supports Abort DMA Mode")
    bm.check_set(iommu.ecap, INTEL_ECAP_PMS_MASK, "Supports Performance Monitoring")
    bm.check_set(iommu.ecap, INTEL_ECAP_SMPWCS_MASK, "Supports snooping of hardware access to paging structures is PWSNP field is set in PASID table entry")
    bm.check_set(iommu.ecap, INTEL_ECAP_FSTS_MASK, "Supports PASID Granular Translation Type of first stage in scalable-mode PASID table entry")
    bm.check_set(iommu.ecap, INTEL_ECAP_SSTS_MASK, "Supports PASID Granular Translation Type of second stage in scalable-mode PASID table entry")
    bm.check_set(iommu.ecap, INTEL_ECAP_SSADS_MASK, "Supports Accessed/Dirty bits in second stage translation")
    bm.check_set(iommu.ecap, INTEL_ECAP_VCS_MASK, "Supports command submission to virtual DMA Remapping hardware")
    bm.check_set(iommu.ecap, INTEL_ECAP_SMTS_MASK, "Supports Scalable Mode DMA Remapping throug scalable-mode context table and PASID table structures)")
    bm.check_set(iommu.ecap, INTEL_ECAP_PDS_MASK, "Supports Page Request Drain flag in invalidation descriptor")
    bm.check_set(iommu.ecap, INTEL_ECAP_DIT_MASK, "Supports Device-TLB invalidation Throttling")
    bm.check_set(iommu.ecap, INTEL_ECAP_PASID_MASK, "Supports Process Address Space IDs (PASID)")

    if bm.bits_check(iommu.ecap, INTEL_ECAP_SMTS_MASK):
        pss = bm.get_bits_shifted(iommu.ecap, INTEL_ECAP_PSS_MASK, INTEL_ECAP_PSS_SHIFT)
        print(f'Supports {pss+1}-bit PASID ids')
        bm.check_set(iommu.ecap, INTEL_ECAP_SRS_MASK, "Supports requests seeking supervisor privilege")
        bm.check_set(iommu.ecap, INTEL_ECAP_MTS_MASK, "Supports Memory type in first stage translation and Extended Memory type in second stage translation")

    bm.check_set(iommu.ecap, INTEL_ECAP_EAFS_MASK, "Supports Extended Access Flags in first stage paging structure entries")

    if bm.bits_check(iommu.ecap, INTEL_ECAP_DT_MASK):
        bm.check_set(iommu.ecap, INTEL_ECAP_NWFS_MASK, "Supports No Write flag in Device-TLB translation requests")

    if bm.bits_check(iommu.ecap, INTEL_ECAP_PASID_MASK):
        bm.check_set(iommu.ecap, INTEL_ECAP_ERS_MASK, "Supports requests-with-PASID seeking execute permission")

    if bm.bits_check(iommu.ecap, INTEL_ECAP_DT_MASK | INTEL_ECAP_SMTS_MASK):
        bm.check_set(iommu.ecap, INTEL_ECAP_PRS_MASK, "Supports Page Requests (PCI PRS)")

    if bm.bits_check(iommu.ecap, INTEL_ECAP_SMTS_MASK | INTEL_ECAP_FSTS_MASK | INTEL_ECAP_SSTS_MASK):
        bm.check_set(iommu.ecap, INTEL_ECAP_NEST_MASK, "Supports nested translations")

    if bm.bits_check(iommu.ecap, INTEL_ECAP_IR_MASK):
        mhmv = bm.get_bits_shifted(iommu.ecap, INTEL_ECAP_MHMV_MASK, INTEL_ECAP_MHMW_SHIFT)
        print(f'Maximum supported value in Interrupt Mask field of Interrupt Entry Cache invalidation descriptor: {mhmv:016x}')

    print(f'IOTLB Register Offset: {bm.get_bits_shifted(iommu.ecap, INTEL_ECAP_IRO_MASK, INTEL_ECAP_IRO_SHIFT):016x}')

    bm.check_set(iommu.ecap, INTEL_ECAP_SC_MASK, "Supports the 1-setting of SNP field in second stage page table entries, and PGSNP field in scalable-mode PASID table entries")
    bm.check_set(iommu.ecap, INTEL_ECAP_PT_MASK, "Supports passthrough translation type in context and scalable-mode pasid table entries")

    bm.check_set(iommu.ecap, INTEL_ECAP_IR_MASK, "Supports Interrupt Remapping")
    if bm.bits_check(iommu.ecap, INTEL_ECAP_IR_MASK):
        bm.check_set_else(iommu.ecap, INTEL_ECAP_EIM_MASK, "Supports 32-bit APIC ids (x2APIC)", "Supports 8-bit APIC ids (xAPIC)")

    bm.check_set(iommu.ecap, INTEL_ECAP_DT_MASK, "Supports Device-TLBs")
    bm.check_set(iommu.ecap, INTEL_ECAP_QI_MASK, "Supports Queued Invalidation")
    bm.check_set_else(iommu.ecap, INTEL_ECAP_C_MASK, "hardware access to remapping structures are coherent", "hardware access to remapping structures are non-coherent")


def walk(dev, args):
    iova = args.iova[0]
    print(f"Walking iova: {iova:016x}")
